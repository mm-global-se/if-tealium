# Tealium

---

[TOC]

## Introduction

Register and initialize Tealium integration.

__NOTE:__ following the naming convension for campaign elements is required (e.g. `a_element: a3_variant`)

## Download

* [tealium-register.js](https://bitbucket.org/mm-global-se/if-tealium/src/master/src/tealium-register.js)

* [tealium-initialize.js](https://bitbucket.org/mm-global-se/if-tealium/src/master/src/tealium-initialize.js)

## Ask client for

+ Campaign Number

+ Campaign Name

## Deployment instructions

### Content Campaign

+ Ensure that you have Integration Factory plugin deployed on site level and has _order: -10_ ([find out more](https://bitbucket.org/mm-global-se/integration-factory))

+ Create another site script with _order: -5_ and add the code from [tealium-register.js](https://bitbucket.org/mm-global-se/if-tealium/src/master/src/tealium-register.js) into it

+ Create campaign script with _order: > -5_, then customize the code from [tealium-initialize.js](https://bitbucket.org/mm-global-se/if-tealium/src/master/src/tealium-initialize.js) and add into it

### Redirect Campaign

Same as for Content Campaign + integration initialization script should be mapped to both, generation and redirect, pages.
