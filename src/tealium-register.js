mmcore.IntegrationFactory.register('Tealium', {
    validate: function (data) {
        if (!data.campaign) {
          return 'No campaign.';
        }

        if (data.campaignNumber) {
          return 'No campaign number.';
        }

        return true;
    },

    check: function (data) {
        return window.utag_data && window.utag && window.utag.link;
    },

    exec: function (data) {
      var elements = data.campaignInfo.replace(/.+?=/, '').split('|'),
          variants = elements.map(function (element) {
            var variantCode = element.split(':')[1].substr(0, 2),
                elementCode = element.substr(0, 1).toUpperCase();

            return variant === 'Default' ? elementCode : variantCode;
          }),

          testCode = data.campaignNumber + '-' + variants.join(','),
          testString = testCode + ' ' + data.campaign;

      if (!data.campaignCode) {
        return;
      }

      window.utag_data.maxymiserLastTest = testString;

      if (window.utag_data.maxymiserTests) {
        if (window.utag_data.maxymiserTests.indexOf(testString) < 0)
          window.utag_data.maxymiserTests += ', ' + testString;
      } else {
        window.utag_data.maxymiserTests = testString;
      }

      window.utag.link({
        category: 'Maxymiser',
        action: data.campaign,
        label: testString
      });

      return true;
    }
});